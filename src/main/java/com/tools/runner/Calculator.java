package com.tools.runner;

import com.tools.Exceptions.CalculatorException;
import com.tools.helpers.CsvHelper;
import com.tools.processors.PostfixCalculatorImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * Singleton driver class to read csv input and output a postfix evaluated csv
 */
public class Calculator {

    private static Calculator instance;

    private Calculator(){}

    public static Calculator getInstance(){
        if (instance == null){
            instance = new Calculator();
        }
        return instance;
    }

    /**
     * Method to read input csv, evaluate expressions and write results csv file
     * @param inputFile input csv file
     * @param outputFile output csv results file
     * @throws CalculatorException if there are any errors during reading, writing or evaluating a postfix notation
     */
    public void calculate(String inputFile, String outputFile) throws CalculatorException {
        List<List<String>> inputRows = CsvHelper.getInstance().readCsv(inputFile);
        List<List<String>> evalRows = new ArrayList<List<String>>();
        for (List<String> row:inputRows){
            ArrayList<String> evalRow = new ArrayList<String>();
            for (String cell:row){
                evalRow.add(evalPostfixNotation(cell,inputRows));
            }
            evalRows.add(evalRow);
        }
        CsvHelper.getInstance().writeCsv(evalRows,outputFile);
    }

    private String evalPostfixNotation(String postfixNotation,List<List<String>> inputRows){
        String result;
        PostfixCalculatorImpl postfixCalculator = new PostfixCalculatorImpl(inputRows);
        try {
            postfixCalculator.calculateNotation(postfixNotation);
            //throw an error if stack is empty or has more than one entry after evaluation
            if (postfixCalculator.getNumStack().empty() || postfixCalculator.getNumStack().size()>1){
                throw new CalculatorException("Error in evaluation of postfix notation");
            }else{
                result = postfixCalculator.getNumStack().pop().toString();
            }
        } catch (CalculatorException e) {
            result = "#ERR";
        }
        return result;
    }
}
