package com.tools.runner;

import com.tools.Exceptions.CalculatorException;
import org.apache.log4j.Logger;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * SpringBoot application class will be the starting point for the program
 */
@SpringBootApplication
public class CommandLineStartupRunner implements CommandLineRunner {
    private static final Logger logger = Logger.getLogger(CommandLineStartupRunner.class);
    private static final String defaultOutputFileName = "output.csv";

    /**
     * Starting method to verify for the required parameters and start the spring boot application
     * @param args arguments passed in commandline
     * @throws Exception if any exception is thrown during execution
     */
    public static void main(String[] args) throws Exception {
        if (args.length == 0 || args.length > 2){
            logger.error(args.length + " inputs provided. Calculator accepts two user inputs, please specify an input csv file path followed by output file path");
        }else {
            SpringApplication app = new SpringApplication(CommandLineStartupRunner.class);
            app.setBannerMode(Banner.Mode.OFF);
            app.run(args);
        }
    }

    /**
     * Invoked by the spring boot application on startup to accept the input csv
     * and output a evaluated postfix notation csv
     * @param args arguments passed in commandline
     * @throws Exception if any exception is thrown during execution
     */
    @Override
    public void run(String... args) throws Exception {
        String inputFile = args[0];
        String outputFile;
        if (args.length == 1){
            outputFile = defaultOutputFileName;
            logger.warn("Output file path not specified. Proceeding with csv file " + outputFile);
        }else{
            outputFile = args[1];
        }
        try {
            Calculator.getInstance().calculate(inputFile,outputFile);
        } catch (CalculatorException e) {
            logger.error(e.getMessage());
        }
    }
}
