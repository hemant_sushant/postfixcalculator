package com.tools.model;

/**
 * Enum with the possible set of operations allowed
 */
public enum Operator {
    ADD("+"), SUBTRACT("-"), MULTIPLY("*"), DIVIDE("/");

    String operatorSign;

    Operator(String operatorSign){
        this.operatorSign = operatorSign;
    }

    public static Operator fromString(String text) {
        for (Operator op : Operator.values()) {
            if (op.operatorSign.equalsIgnoreCase(text)) {
                return op;
            }
        }
        return null;
    }
}
