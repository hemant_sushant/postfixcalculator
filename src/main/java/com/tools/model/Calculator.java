package com.tools.model;

import com.tools.Exceptions.CalculatorException;

/**
 * Interface for implementing a calculator
 */
public interface Calculator {
    void calculateNotation(String notation) throws CalculatorException;
}
