package com.tools.helpers;

import com.tools.Exceptions.CalculatorException;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Singleton helper class to perform operations on csv file/data
 */
public class CsvHelper {
    private static final Logger logger = Logger.getLogger(CsvHelper.class);
    private static CsvHelper instance;

    private CsvHelper(){}

    public static CsvHelper getInstance(){
        if (instance == null){
            instance = new CsvHelper();
        }
        return instance;
    }

    /**
     * parser used to read a csv file and convert it to a collection of postfix notations
     * @param filePath file path for the csv file to be parsed
     * @return Collection of postfix notations
     * @throws CalculatorException when csv file is not present in the path specified
     */
    public List<List<String>> readCsv(String filePath) throws CalculatorException {
        List<List<String>> input = new ArrayList<List<String>>();
        try {
            Reader reader = new FileReader(filePath);
            CSVParser csvParser = new CSVParser(reader, CSVFormat.DEFAULT);

            for (CSVRecord record : csvParser){
                ArrayList<String> row = new ArrayList<String>();
                for (int i=0;i < record.size();i++){
                    row.add(record.get(i).trim().toLowerCase());
                }
                input.add(row);
            }
        }catch(IOException ioe){
            logger.error("Unable to locate csv file in path "+filePath);
            throw new CalculatorException("Unable to locate csv file in path "+filePath);
        }
        return input;
    }

    /**
     * Method to generate a csv file in the file path specified and the collection provided
     * @param outputData Collection that needs to be written into the csv file
     * @param filePath file path to create the csv file
     * @throws CalculatorException if unable to create the csv file in the location specified
     */
    public void writeCsv(List<List<String>> outputData, String filePath) throws CalculatorException {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(filePath));

            CSVPrinter csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT);
            csvPrinter.printRecords(outputData);
            csvPrinter.flush();
        } catch (IOException e) {
            logger.error("Unable to create output csv file in path "+filePath);
            throw new CalculatorException("Unable to create output csv file in path "+filePath);
        }
    }

    /**
     * Gets the value of the corresponding cell as specified by the cell reference
     * @param cellReference cell reference that is being queried
     * @param input Collection from which cell reference value needs to be retrieved
     * @return String value in the cell reference
     * @throws CalculatorException if cell references itself in the value
     */
    public String getCellValue(String cellReference, List<List<String>> input) throws CalculatorException {
        int columnIndex = convertCharToInteger(cellReference.charAt(0));
        int rowIndex = Integer.parseInt(cellReference.substring(1)) - 1;
        if (rowIndex > input.size()-1 || columnIndex > input.get(0).size()-1){
            throw new CalculatorException("Invalid cell reference passed:"+cellReference);
        }
        List<String> row = input.get(rowIndex);
        //failing with error if a cell contains reference to itself
        if (row.get(columnIndex).contains(cellReference)){
            logger.error("Cell referencing itself in the postfix notation in cell "+ cellReference);
            throw new CalculatorException("Cell referencing itself in the postfix notation in cell "+ cellReference);
        }
        return row.get(columnIndex);
    }

    /**
     * method to evaluate a character as an integer to find the column in the csv
     * @param column char value of the column
     * @return integer value of column
     */
    private int convertCharToInteger(char column){
        //converting the cell reference to a number
        return (int)column - (int)'a';
    }
}
