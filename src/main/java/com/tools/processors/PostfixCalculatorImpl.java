package com.tools.processors;

import com.tools.Exceptions.CalculatorException;
import com.tools.helpers.CsvHelper;
import com.tools.model.Operator;
import com.tools.model.Calculator;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Implementation class for evaluating postfix notation
 */
public class PostfixCalculatorImpl implements Calculator {
    private static final Logger logger = Logger.getLogger(PostfixCalculatorImpl.class);
    private Stack<Double> numStack = new Stack<Double>();
    private List<List<String>> inputRows = new ArrayList<List<String>>();
    private static final String cellReferenceRegex = "^[a-z]\\d+$";

    public PostfixCalculatorImpl(List<List<String>> inputRows){
        this.inputRows = inputRows;
    }

    /**
     * Accepts the postfix notation and evaluates it
     * @param postFixString String with postfix notation to evaluate
     * @throws CalculatorException when a postfix string is empty, if notation is invalid or if the operand is invalid
     */
    public void calculateNotation(String postFixString) throws CalculatorException{
        if (postFixString == null || postFixString.isEmpty()){
            logger.error("postFix String is empty, evaluating as zero");
            numStack.push(0.0);
            return;
        }
        String[] rpnArgs = postFixString.split("\\s");
        processArguments(rpnArgs);
    }

    public Stack<Double> getNumStack() {
        return numStack;
    }

    private void processArguments(String[] rpnArgs) throws CalculatorException {
        for (String arg:rpnArgs){
            if(processNumber(arg) != null){
                logger.info("Found a number argument "+arg);
                numStack.push(processNumber(arg));
            }else if(arg.matches(cellReferenceRegex)){
                //checking for format {LETTER}{NUMBER} to compute that cell before evaluating the current
                logger.info("Cell reference " + arg + " found in the postfix notation, evaluating");
                calculateNotation(CsvHelper.getInstance().getCellValue(arg,inputRows));
            }else{
                logger.info("Found a string argument, evaluating as operator "+arg);
                processOperator(arg);
            }
        }
    }

    private void processOperator(String operator) throws CalculatorException {
        if (numStack.size()<2){
            logger.error("Not enough arguments present to perform the operation defined "+operator);
            throw new CalculatorException("Not enough arguments present to perform the operation defined "+operator);
        }

        Double firstOperand = numStack.pop();
        Double secondOperand = numStack.pop();

        if (operator == null || operator.isEmpty() || firstOperand == null || secondOperand == null){
            logger.error("Operator/Operand value not specified");
            throw new CalculatorException("Operator/Operand value not specified");
        }

        Double result = calculate(firstOperand,secondOperand,operator);
        numStack.push(result);
    }

    private Double calculate(Double firstOperand, Double secondOperand, String operator) throws CalculatorException {
        Double result;
        logger.info("Evaluating "+secondOperand+" "+operator+" "+firstOperand);
        if (Operator.fromString(operator) == null){
            logger.error("Invalid Operator passed "+operator);
            throw new CalculatorException("Invalid Operator passed "+operator);
        }
        switch (Operator.fromString(operator)){
            case ADD:
                result = secondOperand + firstOperand;
                break;
            case SUBTRACT:
                result = secondOperand - firstOperand;
                break;
            case MULTIPLY:
                result = secondOperand * firstOperand;
                break;
            case DIVIDE:
                if (firstOperand == 0){
                    logger.error("Cannot divide by zero");
                    throw new CalculatorException("Cannot divide by zero");
                }
                result = secondOperand / firstOperand;
                break;
            default:
                logger.error("Invalid Operator passed "+operator);
                throw new CalculatorException("Invalid Operator passed "+operator);
        }
        return result;
    }

    private Double processNumber(String arg) {
        Double result = null;
        try {
            result = Double.parseDouble(arg);
        } catch (NumberFormatException nfe) {
            //do nothing, string possibly an operator
        }
        return result;
    }
}
