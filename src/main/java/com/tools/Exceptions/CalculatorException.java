package com.tools.Exceptions;

/**
 * Exception class for any errors during execution
 */
public class CalculatorException extends Exception {
    public CalculatorException(String message){
        super(message);
    }
}
