package com.tools.helpers;

import com.tools.Exceptions.CalculatorException;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

/**
 * Test class for CsvHelper
 */
public class CsvHelperTest {
    @Test(expected = CalculatorException.class)
    public void testExceptionOnMissingCsv() throws CalculatorException {
        CsvHelper.getInstance().readCsv("abc");
    }

    @Test
    public void testCsvRead() throws CalculatorException {
        List<List<String>> csvValues = CsvHelper.getInstance().readCsv(getClass().getClassLoader().getResource("testRead.csv").getFile());
        Assert.assertArrayEquals(csvValues.get(0).toArray(),new String[]{"a","b","c"});
    }

    @Test
    public void testGetCellValue() throws CalculatorException {
        List<List<String>> csvValues = Arrays.asList(Arrays.asList("1","2","3"),Arrays.asList("4","5","b2"));
        Assert.assertTrue(CsvHelper.getInstance().getCellValue("a1",csvValues).equals("1"));
        Assert.assertTrue(CsvHelper.getInstance().getCellValue("b2",csvValues).equals("5"));
        Assert.assertTrue(CsvHelper.getInstance().getCellValue("c2",csvValues).equals("b2"));
    }

    @Test(expected = CalculatorException.class)
    public void testGetInvalidColumnCellValue() throws CalculatorException {
        List<List<String>> csvValues = Arrays.asList(Arrays.asList("1","2","3"),Arrays.asList("4","5","6"));
        CsvHelper.getInstance().getCellValue("d1",csvValues);
    }

    @Test(expected = CalculatorException.class)
    public void testGetInvalidRowCellValue() throws CalculatorException {
        List<List<String>> csvValues = Arrays.asList(Arrays.asList("1","2","3"),Arrays.asList("4","5","6"));
        CsvHelper.getInstance().getCellValue("a4",csvValues);
    }

    @Test(expected = CalculatorException.class)
    public void testGetCellValueReferencingItself() throws CalculatorException {
        List<List<String>> csvValues = Arrays.asList(Arrays.asList("a1 1 +","2","3"),Arrays.asList("4","5","6"));
        CsvHelper.getInstance().getCellValue("a1",csvValues);
    }
}
