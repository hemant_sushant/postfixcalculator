package com.tools.runner;

import com.tools.Exceptions.CalculatorException;
import com.tools.helpers.CsvHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;

import java.util.Arrays;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test class for {@link Calculator}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({CsvHelper.class})
public class CalculatorTest {
    @Mock
    CsvHelper csvHelper;

    @Test
    public void testCsvCalculation() throws CalculatorException {
        Whitebox.setInternalState(CsvHelper.class, "instance", csvHelper);
        when(csvHelper.readCsv("sample")).thenReturn(Arrays.asList(Arrays.asList("1","2","3")));
        Calculator.getInstance().calculate("sample","output");
        verify(csvHelper,times(1)).writeCsv(Arrays.asList(Arrays.asList("1.0","2.0","3.0")),"output");
    }
}
