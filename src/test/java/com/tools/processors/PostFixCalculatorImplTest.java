package com.tools.processors;

import com.tools.Exceptions.CalculatorException;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

/**
 * Test Class for PostFixCalculatorImpl.class
 */
public class PostFixCalculatorImplTest {

    @Test
    public void testPostFixCalculationWithoutCellReferences() throws CalculatorException {
        PostfixCalculatorImpl postfixCalculator = new PostfixCalculatorImpl(Arrays.asList(Arrays.asList("1","2","3"),Arrays.asList("4","5","6")));
        postfixCalculator.calculateNotation("3 5 4 + *");
        Assert.assertEquals((Double)27.0, postfixCalculator.getNumStack().pop());
    }

    @Test
    public void testPostFixCalculationWithCellReferences() throws CalculatorException {
        PostfixCalculatorImpl postfixCalculator = new PostfixCalculatorImpl(Arrays.asList(Arrays.asList("1","2","3"),Arrays.asList("4","5","6")));
        postfixCalculator.calculateNotation("3 b1 4 - *");
        Assert.assertEquals((Double)(-6.0), postfixCalculator.getNumStack().pop());
    }

    @Test
    public void testPostFixCalculationWithCellReferencesRecursive() throws CalculatorException {
        PostfixCalculatorImpl postfixCalculator = new PostfixCalculatorImpl(Arrays.asList(Arrays.asList("1","c1","5 a1 +"),Arrays.asList("4","5","6")));
        postfixCalculator.calculateNotation("3 b1 4 - /");
        Assert.assertEquals((Double)(1.5), postfixCalculator.getNumStack().pop());
    }

    @Test(expected = CalculatorException.class)
    public void testPostFixCalculationWithCellReferencesRecursiveError() throws CalculatorException {
        PostfixCalculatorImpl postfixCalculator = new PostfixCalculatorImpl(Arrays.asList(Arrays.asList("0","c1","5 a1 /"),Arrays.asList("4","5","6")));
        postfixCalculator.calculateNotation("3 b1 4 - /");
    }

    @Test(expected = CalculatorException.class)
    public void testPostFixCalculationWithCellReferencingItself() throws CalculatorException {
        PostfixCalculatorImpl postfixCalculator = new PostfixCalculatorImpl(Arrays.asList(Arrays.asList("1","b1 3 +","5 a1 +"),Arrays.asList("4","5","6")));
        postfixCalculator.calculateNotation("3 b1 4 - /");
    }

    @Test
    public void testExceptionWhenInputIsEmpty() throws CalculatorException {
        PostfixCalculatorImpl postfixCalculator = new PostfixCalculatorImpl(Arrays.asList(Arrays.asList("1","2","3"),Arrays.asList("4","5","6")));
        postfixCalculator.calculateNotation("");
        Assert.assertEquals((Double)(0.0),postfixCalculator.getNumStack().pop());
    }

    @Test(expected = CalculatorException.class)
    public void testDivideByZero() throws CalculatorException {
        PostfixCalculatorImpl postfixCalculator = new PostfixCalculatorImpl(Arrays.asList(Arrays.asList("1","2","3"),Arrays.asList("4","5","6")));
        postfixCalculator.calculateNotation("3 5 5 - /");
    }

    @Test(expected = CalculatorException.class)
    public void testInvalidOperatorInPostfix() throws CalculatorException {
        PostfixCalculatorImpl postfixCalculator = new PostfixCalculatorImpl(Arrays.asList(Arrays.asList("1","2","3"),Arrays.asList("4","5","6")));
        postfixCalculator.calculateNotation("3 5 4 6 + &");
    }

    @Test(expected = CalculatorException.class)
    public void testInvalidOperatorsInPostfix() throws CalculatorException {
        PostfixCalculatorImpl postfixCalculator = new PostfixCalculatorImpl(Arrays.asList(Arrays.asList("1","2","3"),Arrays.asList("4","5","6")));
        postfixCalculator.calculateNotation("3 5 4 + * /");
    }

    @Test(expected = CalculatorException.class)
    public void testInvalidOperatorsInPostfix1() throws CalculatorException {
        PostfixCalculatorImpl postfixCalculator = new PostfixCalculatorImpl(Arrays.asList(Arrays.asList("1","2","3"),Arrays.asList("4","5","6")));
        postfixCalculator.calculateNotation("3 5 4 + * /");
    }

}
