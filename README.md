# Postfix Notation Calculator

Commandline program running on spring boot to parse a spreadsheet-like csv file and evaluates each cell by these rules:
1. Each cell is an expression in postfix notation (see Wikipedia).
2. Each token in the expression will be separated by one or more spaces.
3. One cell can refer to another cell with the {LETTER}{NUMBER} notation (e.g. “A2”,
“B4”– letters refer to columns, numbers to rows).
4. Expressions may include the basic arithmetic operators +, -, *, /
Your program should output another CSV file of the same dimensions containing the results
of evaluating each cell to its final value. If any cell is an invalid expression, then for that cell
only print #ERR.

## Getting Started

### Assumptions

1. Input and output files are present/created in the current directory by default. A full/relative path can also be given as arguments to the program.
2. Cell references are in the format {LETTER}{NUMBER} and letter will contain only one character.
3. Extra white spaces in the postfix annotation will be trimmed and before evaluation.
4. Number of elements in each row for the csv file is the same
5. If output file is not specified, default output csv file is created in the working directory as output.csv

### Approach

Calculator is built as a spring boot program to run via commandline from an executable jar.
csv is parsed using commons-csv library and postfix notations are evaluated recursively for each cell within the csv.

### Prerequisites

Before running the command line program, build the project using,

```
mvn clean install
```

## Running the program

To evaluate the csv file with postfix notations, run the below command in commandline from the build directory,

```
java -jar ../src/main/resources/input.csv result.csv
```

The command accepts 2 parameters, a csv input file path and path of evaluated csv file.
If only one parameter is provided, output csv is by default named 'output.csv' and is generated in the working directory.
Any other number of parameters stops execution with an error.



